# basic script written for the purposes of learning commands

print("Hello world!")

name = "Marta"
age = 22
besties = ["Victoria", "Alex", "Veronica"]

print("I'm %s and I'm %d years old." % (name, age))
print("My best friends are: %s" % (', '.join(besties)) +'.')
