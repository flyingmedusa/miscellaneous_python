import csv
import random

names = ["Anna", "Beata", "Katarzyna", "Maciej", "Aleksander", "Klaudia", "Adam", "Wiktoria",
         "Julia", "Michał", "Justyna", "Agnieszka", "Mateusz", "Szymon", "Ewelina", "Agata"]
surnames = ["Nowak", "Zając", "Wójcik", "Król", "Kowalczyk", "Mazur", "Woźniak",
            "Krawczyk", "Kaczmarek", "Wieczorek", "Wróbel", "Stępień"]


def date():
    for _ in range(4):
        year = str(random.randint(1990, 2019))
        month = str(random.randint(1, 12))
        day = str(random.randint(1, 29))

        if len(month) == 1:
            month = str(0) + str(month)
        if len(day) == 1:
            day = str(0) + str(day)
        full_date = year + '-' + month + '-' + day

        return full_date


def phone_num():
    whole_number = ""
    for n in range(11):
        if n == 0:
            whole_number = str(random.randint(5, 9))
        elif n == 3 or n == 7:
            whole_number += " "
        else:
            whole_number += str(random.randint(0, 9))
    number = "+48 " + whole_number
    return number


def email(name, surname):
    domains = ["gmail.com", "outlook.com", "yahoo.com", "wp.pl"]
    full_email = name.lower() + "." + surname.lower() + "@" + random.choice(domains)
    return full_email


with open('customers_data.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["ID", "Data", "Imię", "Nazwisko", "Nr telefonu", "Adres email"])
    for i in range(1, 11):
        name = random.choice(names)
        surname = random.choice(surnames)
        writer.writerow([i, date(), name, surname, phone_num(), email(name, surname)])

